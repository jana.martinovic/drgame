#Globalne varijable i funkcije vezane uz kategorije
import pandas 
import random

MAX_ITER = 5
KATEGORIJE = []
IGRACI = []


def nova_igra(igraci):
    recenice_csv = pandas.read_csv("kategorije.csv", delimiter=(","), header = None, error_bad_lines=False)
    KATEGORIJE.extend(recenice_csv.sample(MAX_ITER).values.tolist())
    IGRACI.extend(igraci)


def nova_kategorija():
    if(KATEGORIJE):
        
        return KATEGORIJE.pop()[0], random.choice(IGRACI)
    else:
        return "None", "None"
    
