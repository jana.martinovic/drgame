import random
import time

MAX_ITER = 5
TASK = ""

def refresh_task(sign, number):
    global TASK
    TASK = "(" + TASK + sign + str(number) + ")"

    if(sign == "*"):
        sign = "×"
    elif(sign == "//"):
        sign = "÷"
    return sign + str(number)

def nova_igra(igraci):
    global TASK, MAX_ITER
    citatelj = random.choice(igraci)
    TASK = "0"
    MAX_ITER = 5

def novi_racunski():
    global TASK, MAX_ITER
    rezultat = int(eval(TASK))
    novi_broj = random.randint(1, 30)
    MAX_ITER -= 1

    if(MAX_ITER < 0):
        print(TASK)
        return "rezultat: "+str(eval(TASK))

    if(rezultat != 0 and rezultat % novi_broj == 0):
        operation = "//"
    
    elif(rezultat != 0 and novi_broj <= 12 and abs(rezultat) <= 12):
        operation = random.choice(["+", "-", "*"])
    else:
        operation = random.choice(["+", "-"])

    return refresh_task(operation, novi_broj)





