# -*- coding: utf-8 -*-

from typing import List
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
import random
import threading


import NeverHaveIEver
import procjene
import kategorije
import alias
import time
import aritmetika


class MainWindow(Screen):
    pass
class NeverHaveIEverMainWindow(Screen):
    pass
class NeverHaveIEverRulesWindow(Screen):
    pass
class NeverHaveIEverWindow(Screen):
    pass
class ProcjeneRulesWindow(Screen):
    pass
class ProcjeneMainWindow(Screen):
    pass
class ProcjeneOdgovorWindow(Screen):
    pass
class ProcjenePitanjeWindow(Screen):
    pass
    pass

class WindowManager(ScreenManager):
    pass
class WindowManager(ScreenManager):
    pass

class KategorijeMainWindow(Screen):
    pass
class KategorijeRulesWindow(Screen):
    pass
class KategorijeWindow(Screen):  
    pass

class AliasMainWindow(Screen):
    pass
class AliasWindow(Screen):
    pass
class AliasWindowPairs(Screen):
    pass
class AliasRulesWindow(Screen):
    pass
class AritmetikaMainWindow(Screen):
    pass
class AritmetikaWindow(Screen):
    pass
class AritmetikaRulesWindow(Screen):
    pass


class MainApp(App):
    redosljed_igara = ListProperty(["procjeneMain", "kategorijeMain", "neverHaveIEverMain","aliasMain", "aritmetikaMain"])
    #redosljed_igara = ListProperty(["aritmetikaMain"])
    recenica = StringProperty()
    zadatak = StringProperty()
    odgovor = StringProperty()
    kategorija = StringProperty()
    rijec = StringProperty()
    igrac_objasnjava_par1 = StringProperty()
    igrac_pogadja_par1 = StringProperty()
    igrac_objasnjava_par2 = StringProperty()
    igrac_pogadja_par2 = StringProperty()
    igrac = StringProperty()
    igraci = ListProperty()
    izraz = StringProperty()
    izrazi = ListProperty()
    
    def build(self):   
        random.shuffle(self.redosljed_igara)
        kv = Builder.load_file("scene.kv") 
        return kv
    
    def sljedeca_igra(self):
        print("redosljed igara: " + str(self.redosljed_igara))
        return self.redosljed_igara.pop(0)
    
    def nova_recenica(self, nova_igra = False):
        if(nova_igra):
           NeverHaveIEver.nova_igra()
            
        recenica = NeverHaveIEver.nova_recenica()
        
        if(recenica == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.recenica = recenica
            
    def nova_procjena(self, nova_igra = False):
        if(nova_igra):
            procjene.nova_igra()     
        zadatak, odgovor = procjene.novi_zadatak()
        
        if(zadatak == "None"):

            app.root.current = self.sljedeca_igra()
        else:
            self.zadatak = zadatak
            self.odgovor = odgovor
            
    def nova_kategorija(self, nova_igra = False):
        if(nova_igra):
            kategorije.nova_igra(self.igraci)
        kategorija, igrac = kategorije.nova_kategorija()
        
        
        if(kategorija == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.kategorija = kategorija
            self.igrac = igrac

    def nova_rijec(self):
        
        rijec = alias.nova_rijec()
       
        print("rijec",rijec)

        if(rijec == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.rijec = rijec
            
        
    def generirajParove(self, nova_igra = False):
        if(nova_igra):
            alias.nova_igra(self.igraci)
        igrac_objasnjava_par1, igrac_objasnjava_par2, igrac_pogadja_par1, igrac_pogadja_par2 = alias.generiraj_parove()

        self.igrac_objasnjava_par1 = igrac_objasnjava_par1
        self.igrac_pogadja_par1 = igrac_pogadja_par1
        self.igrac_objasnjava_par2 = igrac_objasnjava_par2
        self.igrac_pogadja_par2 = igrac_pogadja_par2
    



    def obradaNadimaka(self, text = ""):
        if (not (text[-1].isalpha() or text[-1]=='\n')):
            return text[:-1]
        return text        
    
    def uploadajNadimke(self, nadimci):
        nadimci = nadimci.split("\n")
        self.igraci.extend(nadimci)
        print("Igraci: " + str(self.igraci))


    def pokreniAritmetiku(self):
        aritmetika.nova_igra(["jana", "dora"])
        result = ""
        while(not result.startswith("rezultat")):
            self.izraz = aritmetika.novi_racunski()
            print(result)
            time.sleep(2)
    
    def upd_ltxt(self):
        threading.Thread(target=self.pokreniAritmetiku).start()

        
          
if __name__ == "__main__":
    app = MainApp()
    app.run()