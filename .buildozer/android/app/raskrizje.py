from queue import Queue
import threading
from threading import Lock
import random as rnd
import time
import math
dataI = []
# A thread that produces data
zastavica = True
time_to_sleep = 15
lock = Lock()
stanje_sem = ""
def producer(q1UPR,qThread):
        spavaj = []
        t = threading.current_thread()
        # Produce some data
        data1 = "P-"+rnd.choice(["SZ-SI","SI-SZ","SZ-JZ","JZ-SZ","JZ-JI","JI-JZ","JI-SI","SI-JI"])
        data2 = "A-"+rnd.choice(["S-J","J-S","Z-I","I-Z"])
        global stanje_sem
        choice = rnd.choice([data1,data2])
        t.setName(choice)
        global dataI
        if("P"in choice and choice.replace("P-","") in stanje_sem ):
            
            dataI.append(choice+"|1")
            spavaj.append(choice+"|1")
            
        elif("A" in choice and choice.replace("A-","") in stanje_sem):
            #print("USAO U ELSE")
            dataI.append(choice+"|1")
            spavaj.append(choice+"|1")
           
        else :
            dataI.append(choice+"|0")

        q1UPR.put(choice)

        #javi rasu da prelazis cestu , cekaj 10s
       
        
        
        while True:
            time.sleep(1)
            #print("dretve slusaju q sem:",str(threading.active_count()))
            
            data = qsem.get()
            stanje_sem = data
            l = threading.enumerate()
            lock.acquire()
            for t in range(0,len(dataI)):
                ##il iu for eachu za sve aktivne dretve pa ih sve pusti odjednom!!
                name = dataI[t]
              
                if((name.split("|")[0].replace("P-","") in data and name.split("|")[1]=='0')  or (name.split("|")[0].replace("A-","")in data and name.split("|")[1]=='0')):
                    
                    dataI[dataI.index(name)]= name.replace("0","1")
                    name = name.replace("0","1")
                    
                    spavaj.append(name)
            lock.release()
                    
            time.sleep(5)
            #print("probudio sam se",name)
            if(len(spavaj)!=0):
                for s in spavaj:
                    dataI[dataI.index(s)]=s.replace("1","2")
            spavaj = []
            break

# A thread that consumes data


        
def consumerUPR(q1UPR):
   
    i=1      
    while True:

        data = q1UPR.get()

        lock.acquire()

        global time_to_sleep
        time_to_sleep = 30
        lock.release()
       
        # Process the data
def consumerSEM():
        time_to_sleep1=5
        pjesaci = ["SZ-JZ","JZ-SZ","JI-SI","SI-JI","SZ-SI","SI-SZ","JZ-JI","JI-JZ"]
        auti = ["S-J","J-S","Z-I","I-Z"]
        i = 1;
       
        while (True):
            global time_to_sleep
            poruka = auti[int((i-1)/2)]+auti[int(math.ceil(i/2))]+str(pjesaci[i-1:i+3])
            if(i==5):
                i=1
            else: i = i+4
            global stanje_sem
            stanje_sem = poruka
            #print("stanje sem",stanje_sem)
            

            global dataI
            #print("spavam:",time_to_sleep)
            time.sleep(time_to_sleep)
            #provjeri ima li dretvi korisnika u thred listi, ako nema vrati na staro
            if((len(dataI)==0)):
                
                time_to_sleep = 15
            

def ispis():
    SZ_SI = [5,"|",2,"|",5] ##i obrnuto
    ZI = ["-----",3,"-----"]#ovo 2x
    JZ_JI = [5,"|",2,"|",5]
    lista_p_ZI = ["SI-JI","JI-SI","SZ-JZ","JZ-SZ"]
    lista_p_SJ = ["SI-SZ","SZ-SI","JI-JZ","JZ-JI"]
    lista_a_SJ = ["S-J","J-S"]
    lista_a_ZI = ["Z-I","I-Z"]
    crtajP_ZI = 0
    crtajP_SJ = 0
    crtajA_SJ = 0
    crtajA_ZI = 0
    smjerSJJS = ""
    smjerZIIZ = ""
    while(True):
        global stanje_sem
        global dataI
        print("stanje semafora - zeleno svjetlo:", stanje_sem)
        
        for p in lista_p_ZI:
           
            if ("P-"+p+"|1" in dataI and p in stanje_sem or ("P-"+p+"|0" in dataI and p in stanje_sem)):#|0 je ako je pjesak dosao u trenutku kada je semafor palio zeleno svjetlo i javljao raskrizju te mu semafor nakon toga javio da smije prijec pa da ne kasni preko ceste ako ima dozvolu
                crtajP_ZI = 1
                break;
            if ("P-"+p+"|0" in dataI):
                crtajP_ZI = -1
                break;
        for p in lista_p_SJ:
           # print("Trenutni data",dataI)
            if ("P-"+p+"|1" in dataI and p in stanje_sem or("P-"+p+"|0" in dataI and p in stanje_sem) ):
                crtajP_SJ = 1
                break;
            if ("P-"+p+"|0" in dataI ):
                crtajP_SJ = -1
                break;
        for p in lista_a_SJ:
            #print("Trenutni data",dataI)
            if ("A-"+p+"|1" in dataI and p in stanje_sem or("A-"+p+"|0" in dataI and p in stanje_sem) ):
                crtajA_SJ = 1
                smjerSJJS = p
                break;
            if ("A-"+p+"|0" in dataI ): 
                crtajA_SJ = -1
                break;
        for p in lista_a_ZI:
           # print("Trenutni data",dataI)
            if ("A-"+p+"|1" in dataI and p in stanje_sem or ("A-"+p+"|0" in dataI and p in stanje_sem)):
                crtajA_ZI = 1
                smjerZIIZ = p
                break;
            if ("A-"+p+"|0" in dataI ):
                crtajA_ZI = -1
                break;
        #privjeri jel netko ceka
                   
        
        br = 0
        zas = False
        zas1 = False
        zasIduP = False
        cekaP = False
        cekaPAide = False
        for s in SZ_SI:
            zas = False
            zas1 = False
            if(type(s) == str):
            
                if(crtajP_SJ == 1):
                    print(s+"PP",end = "")
                    crtajP_SJ = 2
                    zasIduP = True
                    zas = True
                    zas1 = True
                if(crtajA_SJ == 1):
                    print(s+"AA",end = "")
                    crtajA_SJ = 2
                    zas = True
                if(crtajP_SJ== -1):
                    if(zas):#ako pjesak ceka i auto ide ili ceka
                        print(s+"P",end = "")  
                        zas1 = True
                        zas = True
                        cekaPAide = True
                    else:
                        print("P"+s,end = "")
                        zas = True
                        zas1 = True
                        cekaP = True
                    crtajP_SJ = 0                    
                if(crtajA_SJ == -1):
                    if(zasIduP!= True and cekaP == False):#ako ne idu pjesaci
                        print(s+"A",end="")
                        zas = True
                    else : print("A",end="")
                    crtajA_SJ = 0
                    
                elif(zas == False and zas1 == False and cekaPAide == False): print(s,end = "")
            else:
                for i in range(0,s): print(" ",end="")
        print("\n")
        zas = False
        br = 0
        
        for s in ZI:

            if(type(s) == str ):
                if(crtajP_ZI == -1):
                   print("P"+s,end = "")
                   crtajA_ZI = 0
                else:
                    print(s,end=" ")
            else:
                for i in range(0,s):
                    
                    print(" ",end="")
        print("\n")
        #crtat na cestu mozes i jednu i drugu stranu za pjesaka jer idu na istu cestu

        if(crtajP_ZI==1):
            print("PPP      PPP")
            print("PPP      PPP")
            crtajP_ZI = 0
        if(crtajA_SJ == 2):
            print("      AA     ")
        if(crtajA_ZI == 1):
            print("AAAAAAAAAA")
            crtajA_ZI = 0
        if(crtajA_ZI == -1):
            print("A")
        for s in ZI:
            br = 0
           
            br = br+1
            if(type(s) == str):
                if(crtajP_ZI == -1):
                   print("P"+s,end = "")
                   crtajP_ZI = 0
                else:
                    print(s,end=" ")

            if(type(s) != str ):
                for i in range(0,s): print(" ",end="")
        print("\n")
        #sad tu provjera za drugu stranu                 
                                        
        zas = False
        br = 0
        for s in JZ_JI:
            br = 0
            zas = False
            br = br+1
            if(type(s) == str ):
                
                if(crtajP_SJ==2):
                    print(s+"PP",end = "")
                    crtajP_SJ = 0
                    zas = True
                if(crtajA_SJ==2):
                    print(s+"AA",end="")
                    crtajA_SJ = 0
                    zas = True
                elif(zas == False): print(s,end = "")

                
            else:
                for i in range(0,s): print(" ",end="")

      
        print("\n")
        print("_____________________________________________________________________")
        time.sleep(3)
# Create the shared queue and launch both threads

qRAS = Queue()
q1UPR = Queue()
qsem = Queue()
qDretve = Queue()
qThread = Queue()
qSEMDraw = Queue()

tCiklus = threading.Thread(target = consumerSEM)
tCiklus.start()
tUPR = threading.Thread(target = consumerUPR, args =(q1UPR,))
tUPR.start()
t5 = threading.Thread(target = ispis)
t5.start()

time.sleep(2)
t2 = threading.Thread(target = producer, args =(q1UPR,qThread ))
t3 = threading.Thread(target = producer, args =(q1UPR,qThread ))
t4 = threading.Thread(target = producer, args =(q1UPR,qThread ))


t2.setName("2")
t2.start()
#t2.join()
time.sleep(1)

#Stime.sleep(1)
t3.start()
#t3.join()
#time.sleep(1)
t4.start()
time.sleep(1)
while(True):
    time.sleep(10)
    t = threading.Thread(target = producer, args =(q1UPR,qThread ))
    t.start()
    #t.join()#blokira ulazak drugih dretvi
#t4.join()

