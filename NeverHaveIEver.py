#Globalne varijable i funkcije vezane uz never have i ever
import pandas 

MAX_ITER = 4
RECENICE = []


def nova_igra():
    global RECENICE
    recenice_csv = pandas.read_csv("recenice_never_have_i_ever.csv", delimiter=(","), header = None)
    RECENICE = []
    RECENICE.extend(recenice_csv.sample(MAX_ITER).values.tolist())


def nova_recenica():
    if(RECENICE):
        return RECENICE.pop()[0]
    else:
        return "None"
    


    