# -*- coding: utf-8 -*-

from glob import glob
from typing import List
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.graphics.svg import Svg
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, BooleanProperty, ObjectProperty
import random
import threading


import NeverHaveIEver
import procjene
import kategorije
import alias
import time
import aritmetika

global_igraci = []
global_citatelj_aritmetike = ""
        
class MainWindow(Screen):
    pass
class DodajIgracaWindow(Screen):
    def on_enter(self):
        self.ids['popis_igraca_label0'].text = ""
        self.ids['popis_igraca_label0'].text = ""
    pass

class NeverHaveIEverMainWindow(Screen):
    pass
class NeverHaveIEverRulesWindow(Screen):
    pass
class NeverHaveIEverWindow(Screen):
    pass
class ProcjeneRulesWindow(Screen):
    pass
class ProcjeneMainWindow(Screen):
    pass
class ProcjeneOdgovorWindow(Screen):
    pass
class ProcjenePitanjeWindow(Screen):
    pass
    pass

class WindowManager(ScreenManager):
    pass


class KategorijeMainWindow(Screen):
    pass
class KategorijeRulesWindow(Screen):
    pass
class KategorijeWindow(Screen):  
    pass

class AliasMainWindow(Screen):
    pass
class AliasWindow(Screen):
    pass
class AliasWindowPairs(Screen):
    pass
class AliasRulesWindow(Screen):
    pass
class AritmetikaMainWindow(Screen):
    def on_enter(self):
        global global_igraci
        global global_citatelj_aritmetike 
        global_citatelj_aritmetike = MainApp.nasumicni_igrac(global_igraci)
        self.ids['aritmetika_citac'].text = "čita " + global_citatelj_aritmetike
    pass
class AritmetikaWindow(Screen):
    def on_enter(self):
        self.ids['aritmetikaBtn'].disabled = True
    pass
class AritmetikaRulesWindow(Screen):
     def on_enter(self):
        self.ids['aritmetika_pravila_citac'].text = "čita " + global_citatelj_aritmetike

class LastWindow(Screen):
    pass


class MainApp(App):
    sve_igre =  ListProperty(["procjeneMain", "kategorijeMain", "neverHaveIEverMain","aliasMain", "aritmetikaMain"])
    redosljed_igara = ListProperty(["procjeneMain", "kategorijeMain", "neverHaveIEverMain","aliasMain", "aritmetikaMain"])
    #redosljed_igara = ListProperty(["procjeneMain"])
    recenica = StringProperty()
    zadatak = StringProperty()
    odgovor = StringProperty()
    kategorija = StringProperty()
    rijec = StringProperty()
    igrac_objasnjava_par1 = StringProperty()
    igrac_pogadja_par1 = StringProperty()
    igrac_objasnjava_par2 = StringProperty()
    igrac_pogadja_par2 = StringProperty()
    igrac = StringProperty()
    igraci = ListProperty()
    izraz = StringProperty()
    izrazi = ListProperty()
    disable_dalje_aritmetika = BooleanProperty()
    
    def build(self):
        self.icon = 'icon.png'
        self.title = "Loop Drinking Game"
        random.shuffle(self.redosljed_igara)
        self.redosljed_igara.append("last")
        kv = Builder.load_file("scene.kv") 
        return kv

    def sljedeca_igra(self):
        print("redosljed igara: " + str(self.redosljed_igara))
        return self.redosljed_igara.pop(0)

    def nova_igra_stari_igraci(self):
        self.redosljed_igara.extend(self.sve_igre)
        random.shuffle(self.redosljed_igara)
        self.redosljed_igara.append("last")
        return self.sljedeca_igra()

    def nova_igra_novi_igraci(self):
        global global_igraci
        self.redosljed_igara.extend(self.sve_igre)
        random.shuffle(self.redosljed_igara)
        self.redosljed_igara.append("last")
        self.igraci = []
        global_igraci = []
        return "main"
    
    def nova_recenica(self, nova_igra = False):
        if(nova_igra):
           NeverHaveIEver.nova_igra()
            
        recenica = NeverHaveIEver.nova_recenica()
        
        if(recenica == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.recenica = recenica
            
    def nova_procjena(self, nova_igra = False):
        if(nova_igra):
            procjene.nova_igra()     
        zadatak, odgovor = procjene.novi_zadatak()
        
        if(zadatak == "None"):

            app.root.current = self.sljedeca_igra()
        else:
            self.zadatak = zadatak
            self.odgovor = odgovor
            
    def nova_kategorija(self, nova_igra = False):
        if(nova_igra):
            kategorije.nova_igra(self.igraci)
        kategorija, igrac = kategorije.nova_kategorija()
        
        
        if(kategorija == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.kategorija = kategorija
            self.igrac = igrac

    def nova_rijec(self):
        rijec = alias.nova_rijec()
       
        print("rijec",rijec)

        if(rijec == "None"):
            app.root.current = self.sljedeca_igra()
        else:
            self.rijec = rijec
            
        
    def generirajParove(self, nova_igra = False):
        if(nova_igra):
            alias.nova_igra(self.igraci)
        igrac_objasnjava_par1, igrac_objasnjava_par2, igrac_pogadja_par1, igrac_pogadja_par2 = alias.generiraj_parove()
        self.igrac_objasnjava_par1 = igrac_objasnjava_par1
        self.igrac_pogadja_par1 = igrac_pogadja_par1
        self.igrac_objasnjava_par2 = igrac_objasnjava_par2
        self.igrac_pogadja_par2 = igrac_pogadja_par2
    
    def nasumicni_igrac(igraci):
        return random.choice(igraci)


    def obradaNadimaka(self, popis_igraca0, popis_igraca1, kreni_button, text = ""):
        if(len(text)>0):
            if (text[-1] == "\n"):
                self.uploadajNadimke(text[:-1])
                popis_igraca0.text, popis_igraca1.text = app.ispisiIgrace()
                kreni_button.disabled = self.dovoljnoIgraca()
                return ""
        
            if (not (text[-1].isalpha())):
                return text[:-1]
        if(len(text) > 12):
            return text[:12]
        return text        
    
    def uploadajNadimke(self, nadimak):
        global global_igraci
        if(len(nadimak) and nadimak not in self.igraci and len(self.igraci) < 20):
            self.igraci.append(nadimak)
            global_igraci.append(nadimak)
            print("Igraci: " + str(self.igraci))
        return len(self.igraci)

    def dovoljnoIgraca(self):
        #javlja gumbu "kreni" kad ga treba enableat
        #na temelju broja igraca
        if(len(self.igraci) >= 4):
            return False
        return True
        
    def ispisiIgrace(self):
        ispis = ""
        ispis0 = ""
        ispis1 = ""
        #ispis u 2 stupca
        if(len(self.igraci) > 10):
            for i in range(0, 10):
                ispis0 += self.igraci[i] + "\n"
            for i in range(10, len(self.igraci)):
                ispis1 += self.igraci[i] + "\n"

            return ispis0, ispis1
        #ispis u 1 stupcu
        else:
            for igrac in self.igraci:
                ispis += igrac + "\n"
        return ispis, ""

    def pokreniAritmetiku(self):
        self.disable_dalje_aritmetika = True
        aritmetika.nova_igra()
        self.izraz = "0"
        while(not self.izraz.startswith("rezultat")):
            time.sleep(4)
            self.izraz = aritmetika.novi_racunski()
        self.disable_dalje_aritmetika = False
        return
        

    
    def upd_ltxt(self):
        th = threading.Thread(target=self.pokreniAritmetiku)
        th.start()
        
          
if __name__ == "__main__":
    app = MainApp()
    app.run()

