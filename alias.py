import pandas 
import random
import os

MAX_ITER = 4
RIJECI = []
IGRACI = []


def nova_igra(igraci):
    global IGRACI
    global RIJECI
    
    rijeci_csv = pandas.read_csv("alias.csv", header = None)
    RIJECI = []
    IGRACI = []
    RIJECI.extend(rijeci_csv.sample(MAX_ITER).values.tolist())
    IGRACI.extend(igraci)
    print(RIJECI)


def nova_rijec():
    if(RIJECI):
        return RIJECI.pop()[0]
    else:
        return "None"

def generiraj_parove():
    podskup = random.sample(IGRACI, 4)
    random.shuffle(podskup)
    return  podskup
