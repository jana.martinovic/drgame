#Globalne varijable i funkcije vezane uz procjene
#lista zadaci je u formatu [[pitanje2, odgovor2], [pitanje2, odgovor2]]
import pandas 

MAX_ITER = 3
ZADACI = []

def nova_igra():
    global ZADACI
    pitanja_csv = pandas.read_csv("procjene.csv", delimiter=(";"), header = None)
    ZADACI = []
    ZADACI.extend(pitanja_csv.sample(MAX_ITER).values.tolist())

def novi_zadatak():
    if (ZADACI):
        pitanje = ZADACI[0][0]
        odgovor = ZADACI[0][1]
        ZADACI.pop(0)
        return pitanje, str(odgovor)
    else:
        return "None", "None"
